module gitlab.com/iMil/gruiddit

require (
	github.com/beefsack/go-rate v0.0.0-20180408011153-efa7637bb9b6 // indirect
	github.com/dghubble/go-twitter v0.0.0-20190719072343-39e5462e111f // indirect
	github.com/dghubble/oauth1 v0.6.0 // indirect
	github.com/jzelinskie/geddit v0.0.0-20190613154237-4b3cacc98c40 // indirect
	github.com/spf13/viper v1.4.0 // indirect
	github.com/thoj/go-ircevent v0.0.0-20190609082534-949efec00844 // indirect
)
