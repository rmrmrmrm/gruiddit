package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
	"github.com/fsnotify/fsnotify"
	"github.com/jzelinskie/geddit"
	"github.com/spf13/viper"
	irc "github.com/thoj/go-ircevent"
)

const (
	newssep = "##"
	newsfmt = "wrong news format"
	noauth  = "is not authorized"
)

type cData struct {
	TwitterCmd    string   `mapstructure:"twittercmd"`
	RedditCmd     string   `mapstructure:"redditcmd"`
	Website       string   `mapstructure:"website"`
	Subreddit     string   `mapstructure:"subreddit"`
	Channel       string   `mapstructure:"channel"`
	IrcServer     string   `mapstructure:"ircserver"`
	IrcName       string   `mapstructure:"ircname"`
	RealName      string   `mapstructure:"realname"`
	IrcPass       string   `mapstructure:"ircpass"`
	AuthUsers     []string `mapstructure:"authusers"`
	TwitterName   string   `mapstructure:"twittername"`
	TAppKey       string   `mapstructure:"tappkey"`
	TAppSecret    string   `mapstructure:"tappsecret"`
	TToken        string   `mapstructure:"ttoken"`
	TTokenSecret  string   `mapstructure:"ttokensecret"`
	RClientId     string   `mapstructure:"rclientid"`
	RClientSecret string   `mapstructure:"rclientsecret"`
	RUserAgent    string   `mapstructure:"ruseragent"`
	RRedirUrl     string   `mapstructure:"rredirurl"`
	RBot          string   `mapstructure:"rbot"`
	RBotPasswd    string   `mapstructure:"rbotpasswd"`
}

var (
	ored              *geddit.OAuthSession
	d                 cData
	lastForwardedTwit Time
)

func getOAuth() *geddit.OAuthSession {
	o, err := geddit.NewOAuthSession(
		d.RClientId,
		d.RClientSecret,
		d.RUserAgent,
		d.RRedirUrl,
	)
	if err != nil {
		log.Fatal(err)
	}
	// Create new auth token for confidential clients (personal scripts/apps).
	err = o.LoginAuth(d.RBot, d.RBotPasswd)
	if err != nil {
		log.Fatal(err)
	}
	return o
}

func waitTweet(irccon *irc.Connection, tclient *twitter.Client) {
	demux := twitter.NewSwitchDemux()
	demux.Tweet = func(tweet *twitter.Tweet) {
		// remove newlines and CRs to avoid passing IRC commands
		twt := strings.ReplaceAll(
			strings.ReplaceAll(tweet.Text, "\n", " "), "\r", "",
		)
		// don't spam the jardin with that noise
		if time.Now().After(lastForwardedTwit.Add(time.Minute * 5)) {
			irccon.Privmsgf(d.Channel, "<@%s> %s", tweet.User.Name, twt)
		}
		lastForwardedTwit = time.Now()
	}
	filterParams := &twitter.StreamFilterParams{
		Track:         []string{d.TwitterName},
		StallWarnings: twitter.Bool(true),
	}
	stream, err := tclient.Streams.Filter(filterParams)
	if err != nil {
		log.Fatal(err)
	}
	demux.HandleChan(stream.Messages)
}

func waitNews(irccon *irc.Connection, tclient *twitter.Client) {

	var last string

	for {
		// cheapest way of staying on the channel
		irccon.Join(d.Channel)

		submissions, err := ored.SubredditSubmissions(
			d.Subreddit,
			geddit.NewSubmissions,
			geddit.ListingOptions{Limit: 1},
		)
		if err != nil {
			log.Println(err)
			// err: oauth2: token expired and refresh token is not set
			// renew token
			ored = getOAuth()
		}

		for _, s := range submissions {
			if last == "" {
				last = s.ID
			}
			if s.ID == last {
				break
			}
			last = s.ID

			irccon.Privmsgf(
				d.Channel,
				"New post on /r/%s! from %s: %s %s%s",
				d.RealName,
				s.Author,
				s.Title,
				d.Website,
				s.Permalink,
			)
			tpost := fmt.Sprintf(
				"%s\n%s%s",
				s.Title,
				d.Website,
				s.Permalink,
			)
			_, _, err := tclient.Statuses.Update(tpost, nil)
			if err != nil {
				log.Print(err)
			}
		}
		time.Sleep(30 * time.Second)
	}
}

func chkUser(irccon *irc.Connection, user string) bool {
	for _, u := range d.AuthUsers {
		if u == user {
			return true
		}
	}
	irccon.Privmsgf(d.Channel, "%s %s", user, noauth)
	return false
}

func postNews(title string, url string) error {
	post := &geddit.NewSubmission{
		Subreddit:   d.Subreddit,
		Title:       title,
		Content:     url,
		Self:        false,
		SendReplies: false,
		Resubmit:    false,
	}
	_, err := ored.Submit(post)
	if err != nil {
		return fmt.Errorf("problem submitting post \"%s\": %s", title, err)
	}
	return nil
}

func privMsg(irccon *irc.Connection, e *irc.Event, tclient *twitter.Client) {
	// don't answer to yourself
	if e.Nick == d.IrcName {
		return
	}
	msg := strings.Split(e.Message(), " ")
	switch msg[0] {
	case d.RedditCmd:
		if !chkUser(irccon, e.Nick) {
			return
		}
		if len(msg) > 1 && msg[1] == "help" {
			irccon.Privmsgf(
				d.Channel,
				"%s <title> %s <link>",
				d.RedditCmd,
				newssep,
			)
			return
		}
		// !gruiddit title ## URL
		if len(msg) < 3 {
			irccon.Privmsg(d.Channel, newsfmt)
			return
		}
		post := strings.Split(strings.Join(msg[1:], " "), newssep)
		if len(post) != 2 {
			irccon.Privmsg(d.Channel, newsfmt)
			return
		}
		err := postNews(
			strings.TrimSpace(post[0]),
			strings.TrimSpace(post[1]),
		)
		if err != nil {
			irccon.Privmsgf(d.Channel, "error posting news: %s", err)
		}
	case d.TwitterCmd:
		if !chkUser(irccon, e.Nick) {
			return
		}
		if len(msg) < 2 {
			irccon.Privmsg(d.Channel, "noting to tweet")
			return
		}
		if msg[1] == "help" {
			irccon.Privmsgf(d.Channel, "%s <tweet>", d.TwitterCmd)
			return
		}
		post := strings.Join(msg[1:], " ")
		_, _, err := tclient.Statuses.Update(post, nil)
		if err != nil {
			log.Print(err)
			irccon.Privmsgf(d.Channel, "error tweeting %s", post)
			return
		}
		irccon.Privmsgf(d.Channel, "tweet posted via @%s", d.TwitterName)
	case "!authusers":
		if !chkUser(irccon, e.Nick) {
			return
		}
		irccon.Privmsg(d.Channel, strings.Join(d.AuthUsers, ", "))
	}
}

func main() {
	viper.SetConfigName(".gruiddit")
	viper.AddConfigPath("$HOME")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err)
	}
	err = viper.Unmarshal(&d)
	if err != nil {
		log.Fatal(err)
	}
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		if e.Op&fsnotify.Write != fsnotify.Write {
			return
		}
		log.Printf("Config file reloaded (%s)", e.Name)
		err = viper.Unmarshal(&d)
		if err != nil {
			log.Println(err)
		}
	})

	// Initialize reddit oauth session
	ored = getOAuth()

	// Initialize twitter session
	config := oauth1.NewConfig(d.TAppKey, d.TAppSecret)
	token := oauth1.NewToken(d.TToken, d.TTokenSecret)
	httpClient := config.Client(oauth1.NoContext, token)
	tclient := twitter.NewClient(httpClient)
	lastForwardedTwit := time.Now()

	irccon := irc.IRC(d.IrcName, d.RealName)
	irccon.Password = d.IrcPass
	//	irccon.VerboseCallbackHandler = true
	//	irccon.Debug = true
	irccon.AddCallback("001", func(e *irc.Event) {
		irccon.Join(d.Channel)
	})
	irccon.AddCallback("PRIVMSG", func(e *irc.Event) {
		privMsg(irccon, e, tclient)
	})
	err = irccon.Connect(d.IrcServer)
	if err != nil {
		log.Fatal(err)
	}

	go waitNews(irccon, tclient)
	go waitTweet(irccon, tclient)

	irccon.Loop()
}
